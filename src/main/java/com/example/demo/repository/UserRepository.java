package com.example.demo.repository;

import com.example.demo.model.User;

import java.util.HashMap;
import java.util.Map;

public class UserRepository {

    public enum ResponseCode {
        UNAUTHORIZED, OK, FORBIDDEN
    }

    private final Map<Integer, User> usersDatabase;

    public UserRepository() {
        usersDatabase = new HashMap<>();

        usersDatabase.put(1, new User("cracker", "cracker1234", true, 0));
        usersDatabase.put(2, new User("marry", "marietta!#09", true, 0));
        usersDatabase.put(3, new User("silver", "$silver$", true, 0));

    }

    public ResponseCode checkLogin(final String login, final String password) {
        for (Map.Entry<Integer, User> entry : usersDatabase.entrySet()) {
            User user = entry.getValue();
            if (!user.isActive()) {
                return ResponseCode.FORBIDDEN;
            }
            if (user.getLogin().equals(login)) {
                if (user.getPassword().equals(password)) {
                    return ResponseCode.OK;
                } else {
                    user.setIncorrectLoginCounter(user.getIncorrectLoginCounter() + 1);
                    if (user.getIncorrectLoginCounter() >= 3) {
                        user.setActive(false);
                        return ResponseCode.UNAUTHORIZED;
                    }
                }

            }
        }
        return ResponseCode.UNAUTHORIZED;
    }
}
